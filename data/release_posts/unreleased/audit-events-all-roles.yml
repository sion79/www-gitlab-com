features:
  secondary:
    - name: "Audit Events now available to Developer+"
      available_in: [premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/administration/audit_events.html#audit-events'
      reporter: mattgonzales
      stage: manage
      categories:
        - 'Audit Events'
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/257514'
      description: |
        Audit logs are one of the most important elements of a system for compliance-minded organizations and their compliance programs. The audit logs provide visibility into user and system activity to help troubleshoot incidents, isolate who was responsible for an action, and allow organizations to compile evidence artifacts for auditors of certain processes. Previously, only Maintainers, Owners, and Administrators could access the [Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html#audit-events) view at their respective levels, which meant all other users were unaware these logs existed and did not have access to view their own activity, let alone maintain a compliance mindset within their organization.

        Now the `Audit Events` view is available from the `Security & Compliance` menu item, and is viewable by users with Developer access and higher. Users with `Developer` access will only be able to see their own activity and not other user activities. (Users with Maintainer access and higher will continue to see both their own activity and other user activities.) This change is intended to make this data more accessible to more users and help strengthen the compliance mindset of GitLab users.
