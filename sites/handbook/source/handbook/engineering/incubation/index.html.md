---
layout: handbook-page-toc
title: Incubation Engineering Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineering Department

The Incubation Engineering Department within the Engineering Division focuses on projects that are pre-Product/Market fit. The projects they work on align with the term "new markets" from one of our [product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from 0 to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnecessarily.

The Department Head is the [VP of Incubation Engineering](/job-families/engineering/vp-of-incubation-engineering/).

## Single-Engineer Groups

- [ML OPs / GitLab Data](/handbook/engineering/incubation/mlops)
- [Monitor APM](/handbook/engineering/incubation/monitor-apm)
- [5 Minute Production App](/handbook/engineering/incubation/5-min-production)
- Real-time Collaboration

