---
layout: handbook-page-toc
title: Monitor APM Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Monitor APM Single-Engineer Group

Monitor APM is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).
Our aim is to integrate monitoring and observability into our DevOps Platform in order to provide a convenient and cost effective solution that allows our customers to monitor the state of their applications, understand how changes they make can impact their applications performance characteristics and give them the tools to resolve issues that arise. This effort will be [SaaS first](https://about.gitlab.com/direction/#saas-first) and we will iterate by leveraging open source agents for auto-instrumentation.

### Vacancy

We’re currently hiring for this role and looking for someone that understands the market, the opportunities and the complexities to help design, and develop our entry into the APM market with an integrated monitoring/observerability tool.  The Senior Fullstack Engineer: APM (Incubation Engineering) will focus on developing new customer facing SaaS monitoring and observability services and rolling out these services out to initial users. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0_2EOavg_XQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

You can apply on our [careers page](https://about.gitlab.com/jobs/careers/).
