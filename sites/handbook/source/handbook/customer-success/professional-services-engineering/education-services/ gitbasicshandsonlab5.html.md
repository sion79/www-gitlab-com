---
layout: handbook-page-toc
title: "GitLab with Git Basics Hands On Guide- Lab 5"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab with Git Basics course."
---
# GitLab with Git Basics Hands On Guide- Lab 5
{:.no_toc}

## LAB 5- AUTO DEVOPS LAB- PREDEFINED TEMPLATE
We will use a pre-defined template for Node JS express to show how auto devops works.  

### Create a New Project with AutoDevOps 
1. Navigate to the **New Project** button and click it. You are now in the new project view.  Navigate to and click on **Create from template** -> locate and select the **NodeJS Express** template -> click **Use template.** 
2. In the Project name dialog box, enter** *autoDevOps-test template* ->  Click **Create project** 
3. Remember auto DevOps is an alternative to crafting your own .gitlab-ci.yml file.  Note the banner alerting us that Auto DevOps is running in the background. 
4. Navigate to the CI/CD in the left pane -> Click **Pipelines** -> note there are no pipelines running.  The common way for a pipeline to execute, is with a commit, so with each commit; a pipeline runs. (default behavior)  
5. Create a **new branch** 
6. Navigate to the left  pane -> Click **Branches** -> Click on **New branch** -> In the Branch name dialog box -> enter  *feature-content-update* -> Click **Create branch** 
7. Navigate to the left pane -> Click on CICD Pipelines -> **Note a a pipeline is running and it is tagged with Auto DevOps** 
8. Click **running** and note the stages. 
9. Make a content change using the **Web IDE** 
10. Navigate to and click on **Repository** - Click **Web-IDE** 
11. Switch to our new feature-content-update branch 
12. Click **views -> index.pug** 
13. Modify the last line of the index.pug file to: ***p GitLab Welcomes you to*** Note: the added “p” before is not an error- this is part of the code. 
14. Click **Commit** ->  add a message: *Updated Index* -> leave the default checkboxes -> Click **commit**. 
15. Assign the merge request to yourself. 
16. Enter *Draft*: in the title -> Navigate to the bottom of page -> Click **Submit merge request**  
Note:  We now have an active merge request and it will show us the status of our pipeline for the last change we made or the last commit we made. 
17. It will deploy into this environment called review/ “branch name”  
18. Click **Packages & Registries** -> **Container Registries** 

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab with Git Basics- please submit your changes via Merge Request!
