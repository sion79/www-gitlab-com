---
layout: handbook-page-toc
title: Finance Growth and Development
---

## Welcome to the GitLab Finance Growth and Development Page
{: .no_toc}

The GitLab Finance Department is dedicated to the growth and development of our team members. To ensure that this is a focus area, we have come up with five programs for FY22.

**On-Boarding Growth and Development**

Finance Team Member on-boarding will include specific tasks and information to foster a culture of continued development. This will include:

-   a list of "Must Meet" for onboarding coffee chats, to be provided by the team member's manager

-   a working style assessment using Simpli5

-   a view into the Finance Career Ladders and a view of the [Org Chart](https://about.gitlab.com/company/team/org-chart/)

-   a link to read GitLab's take on [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#what-is-career-development)

-   an overview of the mentorship programs, shadowing opportunities and [internship programs](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#internship-for-learning)

-   links to the GitLab training tools available from the People group :[External Resources (with no cost)](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#external-resources-some-with-no-cost) &  [Internal Resources](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#internal-resources-1)

**Work Style Assessment**

We will use [Simpli5](https://www.simpli5.com/) assessments to help us develop a standardized set of terms for us to all communicate more efficiently. Simpli5 gives you the power to understand YOUR natural tendencies as well as the natural tendencies of your co-workers. This awareness will help you gain a new perspective of yourself and your teammates.

**Career Plans and Growth Ladders**

Every individual is ultimately responsible for their career plan and managers are responsible for providing an environment to develop a plan. We will work on a structure that clearly lays out a career plan progression and tools to communicate a plan by aligning to the GitLab Career Development [handbook page](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/).

**Cross Training, Mentorship and Shadow Programs**

To align with career development plans, we will create opportunities for cross training with other team members. This will not only allow for knowledge transfer for growth, it will also allow leadership opportunities for team members that wish to train. 
 
With the success of several mentorship programs at GitLab, we will create a similar program for the Finance Org. An example for us to align to is the [Women in Sales Mentorship Program](https://about.gitlab.com/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot).

We will create opportunities to shadow finance leaders for team members to gain perspective and business acumen.

**Training Aligned to GitLab L&D**

To align and support the broader GitLab L&D efforts, we will promote the training made available by the People team. This will allow us to give back and contribute in a way that makes this program even stronger. We will also look for ways to create a curriculum that fits the different areas of the finance org. 


### Finance Mentorship Program
As discussed in [GitLab’s Remote Playbook](https://about.gitlab.com/company/culture/all-remote/), the intentionality behind communication, especially in remote settings, is a critical element of success. This pilot program aims to support GitLab’s communications plan by purposefully creating and cultivating opportunities for mentors, mentees, and connection.

In the finance team, there's a great untapped opportunity for cross-training and relationship building. We are launching our first mentorship pilot program to establish whether mentorship is a good way for the finance team to faciliate learning and development and normalise knowledge sharing in our department.

#### Program Structure

##### 1:1 Coaching Sessions
The program proposes a mentor/mentee relationship between selected applicants on the sales team and mentors in leadership roles across the company. Sessions will take place every other week for 30-minutes and will last for up to 3 months (with the possibility of an extension), as long as both mentors and mentees remain engaged. The mentor/mentee relationship will be cross-divisional, meaning that both parties will have the opportunity to work with and learn from team members outside of your respective divisions.

##### Program Slack Channel
All program communications will be sent through the slack channel [#finance-mentorship-program](https://gitlab.slack.com/archives/C01SELK5M18).

##### Program Timeline

| Phase                             | Timeline                      |
| -------                           | -------                       |
| Call for Mentors and Mentees      | 5 April - 16 April 2021       |
| Pairing process                   | 19 April - 23 April 2021      |
| Mentor program kickoff meeting    | Monday 26th April 2021        |
| Mentorship period                 | 26 April - 30 July 2021     |
| Mid-program feedback survey       | 16 June 2021                |
| End of program feedback survey    | 2 August 2021             |

#### Engagement Criteria
The program will last for up to 3-months if both the mentor and the mentee remain engaged. Being engaged in the program will be defined as:

* Attending all scheduled sessions
* Actively participating in all sessions
* Preparing for calls (mentees will drive agenda)
* Implementing learnings (namely for mentees)

#### Program Participants

| Mentor | Mentee |
| ------- | ------- |
|  |  |

#### Success Metrics
* 80 or greater NPS score across mentors and mentees (9's or 10's for the following question: "Overall, I would recommend this program to another GitLab team member" at the end of the pilot program survey).
* 100% participation in program-related calls

#### DRIs
[@dparker](http://gitlab.com/dparker) and [@bryanwise](https://gitlab.com/bryanwise)

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

